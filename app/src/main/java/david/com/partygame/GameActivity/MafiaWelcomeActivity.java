package david.com.partygame.GameActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import david.com.partygame.R;

/**
 * Created by David on 10/17/2017.
 */

public class MafiaWelcomeActivity extends AppCompatActivity {
	private ActivityState mState = ActivityState.Welcome;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_mafia_welcome);

		setButtonListener();
	}

	private void setButtonListener(){
		Button nextButton = (Button) findViewById(R.id.mafia_initialize_btn_next);
		nextButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(),MafiaInitializeActivity.class);
				startActivity(intent);
			}
		});

	}

	enum ActivityState {Welcome, InitializePlayer}
}
