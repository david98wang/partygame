package david.com.partygame;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import david.com.partygame.GameActivity.MafiaWelcomeActivity;

public class MainActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Intent lauchGame = new Intent(this, MafiaWelcomeActivity.class);
		startActivity(lauchGame);
	}
}
