package david.com.partygame;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

/**
 * Created by David on 10/18/2017.
 */

public class AllMethod {
	public static void showPopUp(View anchorView, Context context){
		View popupView = getLayoutInflater().inflate(R.layout.popup_layout, null);

		PopupWindow popupWindow = new PopupWindow(popupView,
				ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);

		// Example: If you have a TextView inside `popup_layout.xml`
		TextView tv = (TextView) popupView.findViewById(R.id.tv);

		tv.setText("ASDF");

		// Initialize more widgets from `popup_layout.xml`
    ....
    ....

		// If the PopupWindow should be focusable
		popupWindow.setFocusable(true);

		// If you need the PopupWindow to dismiss when when touched outside
		popupWindow.setBackgroundDrawable(new ColorDrawable());

		int location[] = new int[2];

		// Get the View's(the one that was clicked in the Fragment) location
		anchorView.getLocationOnScreen(location);

		// Using location, the PopupWindow will be displayed right under anchorView
		popupWindow.showAtLocation(anchorView, Gravity.NO_GRAVITY,
				location[0], location[1] + anchorView.getHeight());
	}
}
